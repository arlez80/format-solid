# format-solid (Deprecated)

You should use [format-solid2](https://bitbucket.org/arlez80/format-solid2/src/master/) for new project.

3D model format Reader/Writer for Haxe Programming Language.

# Readable formats

## .obj/.mtl

Wavefront Obj

## .mqo

Metasequoia Document / Ver 1.0 and Ver 1.1

## rok

Rokkaku Daioh Format

## .x

Direct X Model File / Text Only

## .fbx

FBX Text Format 6.x ( not tested 7.x )

FBX Binary Format

# Writable formats

## .obj/.mtl

Wavefront Obj

## .mqo

Metasequoia Document / Ver 1.0 and Ver 1.1

## rok

Rokkaku Daioh Format

## .x

Direct X Model File / Text Only

# License

MIT License
