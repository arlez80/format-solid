package format.solid.obj;

import format.solid.Data.SolidFace;
import format.solid.Data.SolidSurface;
import format.solid.obj.Data.OBJ;
import format.solid.obj.Data.OBJGroup;
import haxe.io.Output;

/**
 * Obj Writer
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	/**
	 * 文字列1行書き出す
	 * @param	s
	 */
	private function writeString( s:String ):Void
	{
		this.bo.writeString( s + "\r\n" );
	}

	/**
	 * 引数あり文字列を1行書く
	 * 
	 * 引数が空、nullの場合は書かない
	 * @param	s
	 * @param	a
	 */
	private function writeStringWithArg( s:String, a:String ):Void
	{
		if ( a == null ) return;
		if ( a == "" ) return;
		this.writeString( s + " " + a );
	}

	/**
	 * 書き込む
	 * @param	mqo
	 */
	public function write( obj:OBJ )
	{
		// mtlを書き出す
		this.writeStringWithArg( "mtllib ", obj.mtllib );

		// グループを書きだす
		for( group in obj.groups ) {
			this.writeGroup( group );
		}
	}

	/**
	 * グループ書き出し
	 * @param	group
	 */
	private function writeGroup( group:OBJGroup ):Void
	{
		this.writeString( "g " + group.g );
		this.writeStringWithArg( "usemtl ", group.usemtl );

		// 頂点
		if( group.v != null ) {
			for ( v in group.v ) {
				this.writeString( "v " + v.x + " " + v.y + " " + v.z );
			}
		}

		// テクスチャUV
		if( group.vt != null ) {
			for ( vt in group.vt ) {
				this.writeString( "vt " + vt.u + " " + vt.v );
			}
		}

		// 法線ベクトル
		if( group.vn != null ) {
			for ( vn in group.vn ) {
				this.writeString( "vn " + vn.x + " " + vn.y + " " + vn.z );
			}
		}

		// 頂点インデックス
		if( group.f != null ) {
			for ( f in group.f ) {
				switch( [ f.index, f.uv ] ) {
					case [ SolidSurface.triangle( va, vb, vc ), SolidSurface.triangle( uva, uvb, uvc ) ] :
						this.writeString(
							"f " +
							va + "/" + uva + " " +
							vb + "/" + uvb + " " +
							vc + "/" + uvc
						);
					case [ SolidSurface.rectangle( va, vb, vc, vd ), SolidSurface.rectangle( uva, uvb, uvc, uvd ) ] :
						this.writeString(
							"f " +
							va + "/" + uva + " " +
							vb + "/" + uvb + " " +
							vc + "/" + uvc + " " +
							vd + "/" + uvd
						);
					default:
						throw "Different vertex/uv index size!";
				}
			}
		}
	}
}