package format.solid.obj;

import format.solid.Data;
import format.solid.obj.Data;
import haxe.io.Eof;
import haxe.io.Input;

/**
 * .obj Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	private var bi:Input;

	/**
	 * コンストラクタ
	 * @param	i
	 */
	public function new( bi:Input ) 
	{
		this.bi = bi;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):OBJ
	{
		var mtllib = "";
		var groups = new Array<OBJGroup>( );

		var g = "";
		var usemtl = "";
		var v = new Array<SolidVertex>( );
		var vt = new Array<SolidUV>( );
		var vn = new Array<SolidVertex>( );
		var f = new Array<SolidFace>( );

		var addGroup = function() {
			if ( 0 < f.length ) {
				groups.push( {
					g: g,
					usemtl: usemtl,
					v: v,
					vt: vt,
					vn: vn,
					f: f,
				} );
			}

			g = "";
			usemtl = "";
			v = new Array<SolidVertex>( );
			vt = new Array<SolidUV>( );
			vn = new Array<SolidVertex>( );
			f = new Array<SolidFace>( );
		};

		try {
			while ( true ) {
				var data = this.bi.readLine( ).split( " " );
				switch( data[0] ) {
					case "mtllib":
						mtllib = data[1];
					case "g":
						addGroup( );
						g = data[1];
					case "usemtl":
						usemtl = data[1];
					case "v":
						v.push({ x:Std.parseFloat(data[1]), y:Std.parseFloat(data[2]), z:Std.parseFloat(data[3]) });
					case "vt":
						vt.push({ u:Std.parseFloat(data[1]), v:Std.parseFloat(data[2]) });
					case "vn":
						vn.push({ x:Std.parseFloat(data[1]), y:Std.parseFloat(data[2]), z:Std.parseFloat(data[3]) });
					case "f":
						var a = data[1].split( "/" );
						var b = data[2].split( "/" );
						var c = data[3].split( "/" );
						if( data.length == 4 ) {
							f.push( {
								index: SolidSurface.triangle( Std.parseInt(a[0]), Std.parseInt(b[0]), Std.parseInt(c[0]) ),
								uv: SolidSurface.triangle( Std.parseInt(a[1]), Std.parseInt(b[1]), Std.parseInt(c[1]) )
							});
						}else {
							var d = data[4].split( "/" );
							f.push( {
								index: SolidSurface.rectangle( Std.parseInt(a[0]), Std.parseInt(b[0]), Std.parseInt(c[0]), Std.parseInt(d[0]) ),
								uv: SolidSurface.rectangle( Std.parseInt(a[1]), Std.parseInt(b[1]), Std.parseInt(c[1]), Std.parseInt(d[1]) )
							});
							
						}
					case "#":
						// コメント
					default:
						// unknown
						// throw "unknown data:" + data[0];
				}
			}
		}catch ( e:Eof ) {
			// おわり
			addGroup( );
		}

		return {
			mtllib: mtllib,
			groups: groups,
		};
	}

}