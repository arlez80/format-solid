package format.solid.obj;

import format.solid.Data;

typedef OBJ = {
	mtllib:String,
	groups:Array<OBJGroup>,
};

typedef OBJGroup = {
	g:String,
	usemtl:String,
	v:Array<SolidVertex>,
	vt:Array<SolidUV>,
	vn:Array<SolidVertex>,
	f:Array<SolidFace>,
};
