package format.solid;

import format.solid.Data;

enum Token
{
	TEof;

	TIden( s:String );
	TBegin;
	TEnd;
	TPOpen;
	TPClose;
	TBOpen;
	TBClose;
	TAOpen;
	TAClose;
	TComma;
	TColon;
	TSemicolon;
	CFloat( f:Float );
	CInt( i:Int );
	CString( s:String );
}

/**
 * レキサー
 * @author あるる（きのもと 結衣）
 */
class Lexer
{
	public var src( default, null ):String;
	public var len( default, null ):Int;
	public var line( default, null ):Int;
	public var p:Int;

	/**
	 * コンストラクタ
	 * @param src ソースコード
	 */
	public function new( src:String ) 
	{
		this.src = src;
		this.len = this.src.length;
		this.p = 0;
		this.line = 1;
	}

	/**
	 * error
	 * @param	msg
	 */
	private function error( msg:String ):Void
	{
		throw this.line + ":" + msg;
	}

	/**
	 * トークンがあるか？
	 * @return あるか否か
	 */
	public function hasNext( ):Bool
	{
		this.skipSpace( );
		return ( this.p < this.len );
	}

	/**
	 * コメントとスペースを読みとばす
	 */
	public function skipSpace( ):Void
	{
		var lineComment = false;
		var multiComment = false;

		while ( this.p < this.len ) {
			var through = false;
			var c:String = this.src.charAt( this.p );
			switch( c ) {
				case " ", "\t":
					through = true;
				case "\r":
					lineComment = false;
					through = true;
				case "\n":
					this.line ++;
					lineComment = false;
					through = true;
				case "#":
					lineComment = true;
				case "/":
					if( this.src.charAt( this.p + 1 ) == "/" ) {
						lineComment = true;
					}
			}

			if ( ( !lineComment ) && ( !multiComment ) && ( ! through ) ) break;
			this.p ++;
		}
	}

	/**
	 * トークン取得
	 * @return トークン
	 */
	public function next( ):Token
	{
		this.skipSpace( );
		if ( this.len <= this.p ) return Token.TEof;

		var c:String = this.src.charAt( this.p );

		this.p ++;
		switch( c ) {
			case "{": return Token.TBegin;
			case "}": return Token.TEnd;
			case "(": return Token.TPOpen;
			case ")": return Token.TPClose;
			case "[": return Token.TBOpen;
			case "]": return Token.TBClose;
			case "<": return Token.TAOpen;
			case ">": return Token.TAClose;
			case ",": return Token.TComma;
			case ":": return Token.TColon;
			case ";": return Token.TSemicolon;
			case "\"": return this.getString( );
			case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-":
				return this.getNumber( );
		}

		return this.getIden( );
	}

	/**
	 * 数字取得
	 * @return 数字トークン
	 */
	private function getNumber( ):Token
	{
		var str:StringBuf = new StringBuf( );
		var fFloat = false;

		this.p --;
		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( "-0123456789.xABCDEFabcdef".indexOf( c ) == -1 ) break;
			if ( c == "." ) fFloat = true;
			str.add( c );
			this.p ++;
		}

		try {
			return if ( fFloat ) {
				Token.CFloat( Std.parseFloat( str.toString( ) ) );
			}else {
				Token.CInt( Std.parseInt( str.toString( ) ) );
			};
		}catch ( d:Dynamic ) {
			throw "lexer error: invalid number";
		};
	}

	/**
	 * 文字列取得
	 * @return 数字トークン
	 */
	private function getString( ):Token
	{
		var str:StringBuf = new StringBuf( );

		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( "\"" == c ) break;
			str.add( c );
			this.p ++;
		}
		this.p ++;

		return Token.CString( str.toString( ) );
	}

	/**
	 * 識別子の取得
	 * @return 識別子トークンの取得
	 */
	private function getIden( ):Token
	{
		var str:StringBuf = new StringBuf( );
		str.add( this.src.charAt( this.p - 1 ) );

		// 識別子になりうる文字一覧
		var table:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_-@";

		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( table.indexOf( c ) == -1 ) break;
			str.add( c );
			this.p ++;
		}

		return Token.TIden( str.toString( ) );
	}

	/**
	 * 必要なトークン
	 * @param	t
	 */
	public function expected( e:Token ):Void
	{
		var t = this.next( );
		if ( ! Type.enumEq( t, e ) ) {
			throw "expected :" + Std.string( e ) + " not " + Std.string( t );
		}
	}

	/**
	 * 1つのトークンを浮動小数点として読み込む
	 * @return
	 */
	public function readInteger( ):Int
	{
		var t = this.next( );

		return switch( t ) {
			case Token.CInt( i ): i;
			default:
				throw "invalid integer: " + t;
		};
	}

	/**
	 * 1つのトークンを浮動小数点として読み込む
	 * @return
	 */
	public function readFloat( ):Float
	{
		var t = this.next( );

		return switch( t ) {
			case Token.CFloat( f ): f;
			case Token.CInt( i ): i;
			default:
				throw "invalid number: " + t;
		};
	}

	/**
	 * 1つのトークンを文字列として読み込む
	 * @return
	 */
	public function readString( ):String
	{
		var t = this.next( );

		return switch( t ) {
			case Token.CString( s ): s;
			default:
				throw "invalid string: " + t;
		};
	}

	/**
	 * 1つのトークンを識別子として読み込む
	 * @return
	 */
	public function readIden( ):String
	{
		var t = this.next( );

		return switch( t ) {
			case Token.TIden( s ): s;
			default:
				throw "invalid iden:" + t;
		};
	}
}
