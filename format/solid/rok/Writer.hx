package format.solid.rok;

import format.solid.Data;
import format.solid.rok.Data;
import haxe.io.Output;

/**
 * rok書き出し
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	inline private function writeString( s:String ):Void
	{
		this.bo.writeString( s + "\r" );
	}

	inline private function writeInt( i:Int ):Void
	{
		this.writeString( Std.string(i) );
	}

	inline private function writeFloat( f:Float ):Void
	{
		this.writeString( Std.string(f) );
	}

	/**
	 * 書き込む
	 * @param	mqo
	 */
	public function write( rok:ROK )
	{
		this.writeViewPort( rok );
		this.writePoints( rok );
		this.writeLines( rok );
		this.writeFaces( rok );
		this.writeString( "END00" );

		this.writePalette( rok );
		this.writeBack( rok );
		this.writeViewOption( rok );
		this.writeLineProperty( rok );
	}

	private function writeViewPort( rok:ROK ):Void
	{
		this.writeString( "ROKU4" );

		var tf = rok.viewPort.transform;
		this.writeFloat( tf.pp );
		this.writeFloat( tf.pq );
		this.writeFloat( tf.pr );
		this.writeFloat( tf.dx );
		this.writeFloat( tf.dy );
		this.writeFloat( tf.dz );
		this.writeFloat( tf.bx );
		this.writeFloat( tf.by );
		this.writeInt( tf.pers ? 1 : 0 );

		switch( rok.viewPort.viewMode ) {
			case normal: this.writeInt( 0 );
			case yz: this.writeInt( 1 );
			case zx: this.writeInt( 2 );
			case xy: this.writeInt( 3 );
		}

		var scale = rok.viewPort.scale2D;
		this.writeFloat( scale.x );
		this.writeFloat( scale.y );
		var translate = rok.viewPort.translate2D;
		this.writeFloat( translate.x );
		this.writeFloat( translate.y );
	}

	private function writePoints( rok:ROK ):Void
	{
		this.writeString( "POINT" );

		for( t in rok.points ) {
			this.writeInt( t.id );
			this.writeInt( t.groupId );
			this.writeInt( t.visible ? 0 : 1 );
			this.writeInt( t.dummy );
			this.writeInt( t.mirrorPointId );
			this.writeFloat( t.position.x );
			this.writeFloat( t.position.y );
			this.writeFloat( t.position.z );
		}
	}

	private function writeLines( rok:ROK ):Void
	{
		this.writeString( "LINE0" );

		for( t in rok.lines ) {
			this.writeInt( t.from );
			this.writeInt( t.to );
			this.writeInt( t.groupId );
			this.writeInt( t.visible ? 0 : 1 );
		}
	}

	private function writeFaces( rok:ROK ):Void
	{
		this.writeString( "FACE0" );

		for( t in rok.faces ) {
			this.writeInt( t.count );
			this.writeInt( t.color );
			this.writeInt( t.id );
			for( n in t.lines ) {
				this.writeInt( n );
			}
		}
	}

	private function writePalette( rok:ROK ):Void
	{
		if ( rok.palette == null ) return;

		var p = rok.palette;
		this.writeString( "PALC0" );
		this.writeInt( p.count );
		this.writeInt( p.selected );
		for( t in p.colors ) {
			this.writeInt( t.bright.r );
			this.writeInt( t.bright.g );
			this.writeInt( t.bright.b );
			this.writeInt( t.dark.r );
			this.writeInt( t.dark.g );
			this.writeInt( t.dark.b );
			this.writeInt( 0 );
		}
		this.writeString( "ENDP0" );
	}

	private function writeBack( rok:ROK ):Void
	{
		if ( rok.back == null ) return;

		var p = rok.back;
		this.writeString( "BAKC0" );
		this.writeInt( p.count );
		this.writeInt( p.selected );
		for( t in p.colors ) {
			this.writeInt( t.r );
			this.writeInt( t.g );
			this.writeInt( t.b );
			this.writeInt( 0 );
		}
		this.writeString( "ENDB0" );
	}

	private function writeLights( rok:ROK ):Void
	{
		if ( rok.lights == null ) return;

		var p = rok.lights;
		this.writeString( "LIT00" );
		this.writeInt( p.count );
		this.writeInt( p.selected );
		for( t in p.positions ) {
			this.writeFloat( t.x );
			this.writeFloat( t.y );
			this.writeFloat( t.z );
			this.writeInt( 0 );
		}
		this.writeString( "ENDL0" );
	}

	private function writeViewOption( rok:ROK ):Void
	{
		if ( rok.viewOption == null ) return;

		var p = rok.viewOption;
		this.writeString( "VIEW0" );
		this.writeInt( p.visibleLine ? 1 : 0 );
		this.writeInt( p.visibleFace ? 1 : 0 );
		this.writeInt( 0 );
		this.writeInt( 0 );
		this.writeString( "ENDV0" );
	}

	private function writeLineProperty( rok:ROK ):Void
	{
		var found = false;
		for ( t in rok.lines ) {
			if( t.property != null ) {
				found = true;
				break;
			}
		}
		if ( ! found ) return;

		this.writeString( "LINE2" );
		for ( id in 0 ... rok.lines.length ) {
			var t = rok.lines[id];
			if( t.property != null ) {
				this.writeInt( id + 1 );
				this.writeInt( t.property.type );
				this.writeInt( t.property.color );
				this.writeInt( t.property.thin );
			}
		}
		this.writeString( "ENDL2" );
	}
}
