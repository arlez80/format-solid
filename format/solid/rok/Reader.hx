package format.solid.rok;

import haxe.io.Input;
import format.solid.Data;
import format.solid.rok.Data;

/**
 * ROK
 * @author あるる（きのもと 結衣）
 */
class Reader 
{
	private var iter:Iterator<String>;

	/**
	 * コンストラクタ
	 * @param	bi
	 */
	public function new( bi:Input ) 
	{
		this.iter = bi.readAll( ).toString( ).split( "\r" ).iterator( );
	}

	private function readInt( ):Int
	{
		return Std.parseInt( iter.next( ).split( " " ).join( "" ) );
	}

	private function readFloat( ):Float
	{
		return Std.parseFloat( iter.next( ).split( " " ).join( "" ) );
	}

	private function readColor( ):SolidColor3Int
	{
		var r = this.readInt( );
		var g = this.readInt( );
		var b = this.readInt( );
		return { r:r, g:g, b:b };
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):ROK
	{
		var rok:ROK = {
			viewPort: null,
			points: null,
			lines: null,
			faces: null,
			palette: null,
			back: null,
			lights: null,
			viewOption: null,
		};

		if ( this.iter.next( ) != "ROKU4" ) throw "invalid header";
		rok.viewPort = this.readViewPort( );
		rok.points = this.readPoints( );
		rok.lines = this.readLines( );
		rok.faces = this.readFaces( );

		// あれば差し替える
		while ( this.iter.hasNext( ) ) {
			var s = this.iter.next( );
			switch( s ) {
				case "PALC0": rok.palette = this.readPalette( );
				case "BAKC0": rok.back = this.readBack( );
				case "LIT00": rok.lights = this.readLights( );
				case "VIEW0": rok.viewOption = this.readViewOption( );
				case "LINE2": this.readLineProperty( rok );
			}
		}

		return rok;
	}

	private function readViewPort( ):ROKViewPort
	{
		var pp = this.readFloat( );
		var pq = this.readFloat( );
		var pr = this.readFloat( );
		var dx = this.readFloat( );
		var dy = this.readFloat( );
		var dz = this.readFloat( );
		var bx = this.readFloat( );
		var by = this.readFloat( );
		var pers = this.readInt( ) == 1;
		var viewMode = [ normal, yz, zx, xy ][this.readInt( )];
		var scale2Dx = this.readFloat( );
		var scale2Dy = this.readFloat( );
		var translate2Dx = this.readFloat( );
		var translate2Dy = this.readFloat( );
		
		return {
			transform: {
				pp: pp, pq: pq, pr: pr,
				dx: dx, dy: dy, dz: dz,
				bx: bx, by: by,
				pers: pers
			},
			viewMode: viewMode,
			scale2D: { x:scale2Dx, y:scale2Dy },
			translate2D: { x:translate2Dx, y:translate2Dy },
		};
	}

	private function readPoints( ):Array<ROKPoint>
	{
		if ( this.iter.next( ) != "POINT" ) throw "invalid header";

		var result = new Array<ROKPoint>( );

		while ( this.iter.hasNext( ) ) {
			var s = this.iter.next( );
			if ( s == "LINE0" ) break;

			var id = Std.parseInt( s.split( " " ).join( "" ) );
			var groupId = this.readInt( );
			var visible = this.readInt( ) == 0;
			var dummy = this.readInt( );
			var mirrorPointId = this.readInt( );
			var x = this.readFloat( );
			var y = this.readFloat( );
			var z = this.readFloat( );

			result.push({
				id: id,
				groupId: groupId,
				visible: visible,
				dummy: dummy,
				mirrorPointId: mirrorPointId,
				position: { x:x, y:y, z:z },
			});
		}

		return result;
	}

	private function readLines( ):Array<ROKLine>
	{
		var result = new Array<ROKLine>( );

		while ( this.iter.hasNext( ) ) {
			var s = this.iter.next( );
			if ( s == "FACE0" ) break;

			var from = Std.parseInt( s.split( " " ).join( "" ) );
			var to = this.readInt( );
			var groupId = this.readInt( );
			var visible = this.readInt( ) == 0;

			result.push({
				from: from,
				to: to,
				groupId: groupId,
				visible: visible,
				property: null,
			});
		}

		return result;
	}

	private function readFaces( ):Array<ROKFace>
	{
		var result = new Array<ROKFace>( );

		while ( this.iter.hasNext( ) ) {
			var s = this.iter.next( );
			if ( s == "END00" ) break;

			var count = Std.parseInt( s.split( " " ).join( "" ) );
			var color = this.readInt( );
			var id = this.readInt( );
			var lines = new Array<Int>( );
			for( i in 0 ... count ) {
				lines.push( this.readInt( ) );
			}

			result.push({
				count: count,
				color: color,
				id: id,
				lines: lines,
			});
		}

		return result;
	}

	private function readPalette( ):ROKPalette
	{
		var count = this.readInt( );
		var selected = this.readInt( );
		var colors = new Array<ROKColor>( );
		for( i in 0 ... count ) {
			var bright = this.readColor( );
			var dark = this.readColor( );
			this.readInt( );
			colors.push({
				bright: bright,
				dark: dark
			});
		}

		if ( this.iter.next( ) != "ENDP0" ) throw "invalid end of chunk";

		return {
			count: count,
			selected: selected,
			colors: colors,
		};
	}

	private function readBack( ):ROKBack
	{
		var count = this.readInt( );
		var selected = this.readInt( );
		var colors = new Array<SolidColor3Int>( );
		for ( i in 0 ... count ) {
			colors.push( this.readColor( ) );
			this.readInt( );
		}

		if ( this.iter.next( ) != "ENDB0" ) throw "invalid end of chunk";

		return {
			count: count,
			selected: selected,
			colors: colors,
		};
	}

	private function readLights( ):ROKLight
	{
		var count = this.readInt( );
		var selected = this.readInt( );
		var positions = new Array<SolidVertex>( );
		for ( i in 0 ... count ) {
			var x = this.readFloat( );
			var y = this.readFloat( );
			var z = this.readFloat( );
			positions.push({ x:x, y:y, z:z });
			this.readInt( );
		}

		if ( this.iter.next( ) != "ENDL0" ) throw "invalid end of chunk";

		return {
			count: count,
			selected: selected,
			positions: positions,
		};
	}

	private function readViewOption( ):ROKViewOption
	{
		var visibleLine = this.readInt( ) == 1;
		var visibleFace = this.readInt( ) == 1;
		this.readInt( );
		this.readInt( );

		if ( this.iter.next( ) != "ENDV0" ) throw "invalid end of chunk";

		return {
			visibleLine: visibleLine,
			visibleFace: visibleFace,
		};
	}

	private function readLineProperty( rok:ROK ):Void
	{
		while ( this.iter.hasNext( ) ) {
			var s = this.iter.next( );
			if ( s == "ENDL2" ) break;

			var id = Std.parseInt( s.split( " " ).join( "" ) );
			var type = this.readInt( );
			var color = this.readInt( );
			var thin = this.readInt( );
			rok.lines[id - 1].property = {
				type: type,
				color: color,
				thin: thin,
			};
		}
	}
}
