package format.solid;

enum SolidAxis {
	X_UP;
	Y_UP;
	Z_UP;
}

typedef SolidVertex = {
	x:Float, y:Float, z:Float,
};

typedef SolidUV = {
	u:Float, v:Float,
};

typedef SolidFace = {
	index:SolidSurface,
	uv:SolidSurface,
};

enum SolidSurface {
	triangle( a:Int, b:Int, c:Int );
	rectangle( a:Int, b:Int, c:Int, d:Int );
}

typedef SolidColor3Int = {
	r:Int, g:Int, b:Int
};

typedef SolidColor4Int = {
	r:Int, g:Int, b:Int
};

typedef SolidColor3 = {
	r:Float, g:Float, b:Float
};

typedef SolidColor4 = {
	r:Float, g:Float, b:Float, a:Float
};

