package format.solid.mtl;

import format.solid.Data;

typedef MTL = {
	materials:Map<String,MTLMaterial>
};

typedef MTLMaterial = {
	name:String,
	Ka:SolidColor3,
	Kd:SolidColor3,
	Ks:SolidColor3,
	d:Float,
	illum:Int,
	map_Ka:String,
	map_Kd:String,
	map_d:String,
	map_Ks:String,
	map_bump:String,
};

