package format.solid.mtl;

import format.solid.Data;
import format.solid.mtl.Data;
import haxe.io.Output;

/**
 * MTL Writer
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	/**
	 * 文字列1行書き出す
	 * @param	s
	 */
	private function writeString( s:String ):Void
	{
		this.bo.writeString( s + "\r\n" );
	}

	/**
	 * 引数あり文字列を1行書く
	 * 
	 * 引数が空、nullの場合は書かない
	 * @param	s
	 * @param	a
	 */
	private function writeStringWithArg( s:String, a:String ):Void
	{
		if ( a == null ) return;
		if ( a == "" ) return;
		this.writeString( s + " " + a );
	}

	/**
	 * 書き込む
	 * @param	mqo
	 */
	public function write( mtl:MTL )
	{
		for( group in mtl.materials ) {
			this.writeGroup( group );
		}
	}

	/**
	 * グループ書き出し
	 * @param	group
	 */
	private function writeGroup( group:MTLMaterial ):Void
	{
		this.writeString( "newmtl " + group.name );

		if( group.Ka != null ) {
			this.writeString( "Ka " + group.Ka.r + " " + group.Ka.g + " " + group.Ka.b );
		}
		if( group.Kd != null ) {
			this.writeString( "Kd " + group.Kd.r + " " + group.Kd.g + " " + group.Kd.b );
		}
		if( group.Ks != null ) {
			this.writeString( "Ks " + group.Ks.r + " " + group.Ks.g + " " + group.Ks.b );
		}
		this.writeString( "d " + group.d );
		this.writeString( "illum " + group.illum );
		this.writeStringWithArg( "map_Ka ", group.map_Ka );
		this.writeStringWithArg( "map_Kd ", group.map_Kd );
		this.writeStringWithArg( "map_d ", group.map_d );
		this.writeStringWithArg( "map_Ks ", group.map_Ks );
		this.writeStringWithArg( "map_bump ", group.map_bump );
	}
}