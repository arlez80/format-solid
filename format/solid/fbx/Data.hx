package format.solid.fbx;

import haxe.Int64;
import haxe.io.Bytes;

/**
 * FBX ノード
 */
typedef FBXNode = {
	name:String,
	properties:Array<FBXProperty>,
	children:Array<FBXNode>,
};

/**
 * FBX プロパティ
 */
enum FBXProperty {
	PBool( b:Bool );
	PInteger( i:Int64 );
	PFloat( f:Float );
	PString( s:String );
	PIden( t:String );
	PArray( ai:Array<FBXProperty> );
	PBinary( b:Bytes );
}
