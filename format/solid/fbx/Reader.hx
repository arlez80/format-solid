package format.solid.fbx;

import format.solid.fbx.Data;
import format.solid.fbx.FBXLexer;
import haxe.Int64;
import haxe.io.Bytes;
import haxe.io.BytesInput;
import haxe.io.Input;
import haxe.zip.InflateImpl;

/**
 * FBX Reader
 * @author あるる（きのもと 結衣）
 */
class Reader 
{
	private var bytes:Bytes;

	/**
	 * コンストラクタ
	 * @param	bi	入力
	 */
	public function new( bi:Input ) 
	{
		this.bytes = bi.readAll( );
	}

	/**
	 * 読み込む
	 */
	public function read( ):FBXNode
	{
		var bi = new BytesInput( this.bytes );

		// バイナリチェック
		return if ( this.bytes.getString( 0, 20 ) == "Kaydara FBX Binary  " ) {
			var reader = new BinaryFormatReader( bi );
			reader.read( );
		}else {
			var reader = new TextFormatReader( bi );
			reader.read( );
		};
	}
}

/**
 * FBX Text Format Reader
 */
class TextFormatReader
{
	private var lex:FBXLexer;

	/**
	 * コンストラクタ
	 * @param	bi	入力
	 */
	public function new( bi:Input ) 
	{
		this.lex = new FBXLexer( bi.readAll( ).toString( ) );
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):FBXNode
	{
		return this.readNode( "root" );
	}

	/**
	 * ノードを読み込む
	 * @return
	 */
	public function readNode( name:String ):FBXNode
	{
		var properties = new Array<FBXProperty>( );
		var children = new Array<FBXNode>( );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case FBXToken.TIden( s ):
					this.lex.expected( FBXToken.TColon );
					var bkp = this.lex.p;
					var t = this.lex.next( );
					switch( t ) {
						case FBXToken.TBegin:
							children.push( this.readNode( s ) );
						case FBXToken.TIden( _ ):
							// Nullノード
							if ( this.lex.next( ) == FBXToken.TColon ) {
								this.lex.p = bkp;
								children.push( { name: "", properties: [], children: [] } );
							}else {
								// 古い配列の可能性がある
								this.lex.p = bkp;
								var d = this.readOldArray( );
								properties.push( d.prop );
								if ( d.child != null ) {
									d.child.name = s;
									children.push( d.child );
								}
							}
						case FBXToken.CInt( _ ), FBXToken.CFloat( _ ), FBXToken.CString( _ ):
							// 古い配列の可能性がある
							this.lex.p = bkp;
							var d = this.readOldArray( );
							properties.push( d.prop );
							if ( d.child != null ) {
								d.child.name = s;
								children.push( d.child );
							}
						case FBXToken.TStar:
							properties.push( this.readNewArray( ) );
						default:
							throw "invalid token: " + t;
					}
				case FBXToken.TEnd, FBXToken.TEof:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return {
			name: name,
			properties: properties,
			children: children,
		};
	}

	/**
	 * あたらしい配列
	 * @return
	 */
	private function readNewArray( ):FBXProperty
	{
		throw "not implemented";
	}

	/**
	 * 古い配列を読み込む
	 * @return
	 */
	private function readOldArray( ):{ prop:FBXProperty, child:FBXNode }
	{
		var a = new Array<FBXProperty>( );
		var foundNode = false;

		while ( this.lex.hasNext( ) ) {
			// データ取得
			var bkp = this.lex.p;
			var t = this.lex.next( );
			switch( t ) {
				case FBXToken.CInt( i ): a.push( FBXProperty.PInteger( i ) );
				case FBXToken.CFloat( f ): a.push( FBXProperty.PFloat( f ) );
				case FBXToken.CString( s ): a.push( FBXProperty.PString( s ) );
				case FBXToken.TIden( s ):
					var c = s.charCodeAt( 0 );
					a.push( FBXProperty.PBool( c & 1 == 1 ) );
				default:
					throw "invalid token: " + t;
			}
			// 続くか？
			var bkp = this.lex.p;
			var t = this.lex.next( );
			switch( t ) {
				case FBXToken.TComma:
					// ok
				case FBXToken.TIden( _ ), FBXToken.TEnd:
					this.lex.p = bkp;
					break;
				case FBXToken.TBegin:
					foundNode = true;
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		var child = if ( foundNode ) {
			this.readNode( "" );
		}else {
			null;
		};

		return {
			prop: ( a.length == 1 ) ? a[0] : FBXProperty.PArray( a ),
			child: child,
		};
	}
}

/**
 * FBX Binary Format Reader
 */
class BinaryFormatReader
{
	private var bytes:Bytes;
	private var pos:Int;
	private var version:Int;

	/**
	 * コンストラクタ
	 * @param	bi	入力
	 */
	public function new( bi:Input ) 
	{
		this.bytes = bi.readAll( );
		this.pos = 0;
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):FBXNode
	{
		this.readHeader( );

		var children = new Array<FBXNode>( );
		while ( true ) {
			var node = this.readNode( );
			if ( node == null ) break;
			children.push( node );
		}

		return {
			name: "root",
			properties: [],
			children: children,
		};
	}

	/**
	 * ヘッダ読み込み
	 */
	private function readHeader( ):Void
	{
		try {
			if ( this.biReadString( 20 ) != "Kaydara FBX Binary  " ) throw "";
			if ( this.biReadByte( ) != 0 ) throw "";
			if ( this.biReadByte( ) != 0x1a ) throw "";
			if ( this.biReadByte( ) != 0x00 ) throw "";

			this.version = this.biReadInt32( );
		}catch ( d:Dynamic ) {
			throw "invalid header";
		}
	}

	/**
	 * バイナリを読み込む
	 * @return
	 */
	private function biRead( len:Int ):Bytes
	{
		var a = this.bytes.sub( pos, len );
		pos += len;
		return a;
	}

	/**
	 * 整数を読み込む
	 * @return
	 */
	private function biReadByte( ):Int
	{
		var a = this.bytes.get( pos );
		pos ++;
		return a;
	}

	/**
	 * 整数を読み込む
	 * @return
	 */
	private function biReadInt16( ):Int
	{
		var a = this.bytes.getInt32( pos ) & 0xffff;
		pos += 2;
		return a;
	}

	/**
	 * 整数を読み込む
	 * @return
	 */
	private function biReadInt32( ):Int
	{
		var a = this.bytes.getInt32( pos );
		pos += 4;
		return a;
	}

	/**
	 * 整数を読み込む
	 * @return
	 */
	private function biReadInt64( ):Int64
	{
		var a = this.bytes.getInt64( pos );
		pos += 8;
		return a;
	}

	/**
	 * 単精度浮動小数を読み込む
	 * @return
	 */
	private function biReadFloat( ):Float
	{
		var a = this.bytes.getFloat( pos );
		pos += 4;
		return a;
	}

	/**
	 * 倍精度浮動小数を読み込む
	 * @return
	 */
	private function biReadDouble( ):Float
	{
		var a = this.bytes.getDouble( pos );
		pos += 8;
		return a;
	}

	/**
	 * 文字列を読み込む
	 * @return
	 */
	private function biReadString( len:Int ):String
	{
		var a = this.bytes.getString( pos, len );
		pos += len;
		return a;
	}

	/**
	 * 32/64bitの整数を読み込む
	 * @return
	 */
	private function biReadInteger( ):Int64
	{
		return if ( this.version < 7500 ) {
			this.biReadInt32( );
		}else {
			this.biReadInt64( );
		};
	}

	/**
	 * ノードを読み込む
	 * @return
	 */
	private function readNode( ):FBXNode
	{
		var offset = this.biReadInteger( );
		var propertyCount = this.biReadInteger( );
		var propertyBytes = this.biReadInteger( );
		var nameLength = this.biReadByte( );
		// NULLノード
		if (
			( offset.high == 0 ) && ( offset.low == 0 ) &&
			( propertyCount.high == 0 ) && ( propertyCount.low == 0 ) &&
			( propertyBytes.high == 0 ) && ( propertyBytes.low == 0 ) &&
			( nameLength == 0 ) 
		) return null;

		// 名前
		var name = this.biReadString( nameLength );

		// プロパティリスト
		var properties = this.readProperties( propertyCount );

		// 子ノード
		var children = new Array<FBXNode>( );
		if( 0 < Int64.compare( offset, this.pos ) ) {
			while ( true ) {
				var node = this.readNode( );
				if ( node == null ) break;
				children.push( node );
			}
		}

		return {
			name: name,
			properties: properties,
			children: children,
		};
	}

	/**
	 * プロパティ一覧を読み込む
	 * @param	count	プロパティ数
	 * @return
	 */
	private function readProperties( count:Int64 ):Array<FBXProperty>
	{
		var a = new Array<FBXProperty>( );

		while ( 0 < Int64.compare( count, 0 ) ) {
			a.push( this.readProperty( ) );

			count = Int64.sub( count, 1 );
		}

		return a;
	}

	/**
	 * プロパティを1つ読み込む
	 * @return
	 */
	private function readProperty( ):FBXProperty
	{
		var c = this.biReadString( 1 );
		return switch( c ) {
			case "C": FBXProperty.PBool( this.biReadByte( ) & 1 == 1 );
			case "Y": FBXProperty.PInteger( this.biReadInt16( ) );
			case "I": FBXProperty.PInteger( this.biReadInt32( ) );
			case "L": FBXProperty.PInteger( this.biReadInt64( ) );
			case "F": FBXProperty.PFloat( this.biReadFloat( ) );
			case "D": FBXProperty.PFloat( this.biReadDouble( ) );
			case "R":
				var length = this.biReadInt32( );
				FBXProperty.PBinary( this.biRead( length ) );
			case "S":
				var length = this.biReadInt32( );
				var s = this.biReadString( length );
				FBXProperty.PString( s );
			case "b", "i", "l", "f", "d":
				var length = this.biReadInt32( );
				var encoded = this.biReadInt32( );
				var byteLength = this.biReadInt32( );
				var data = if ( encoded == 0 ) {
					new BytesInput( this.biRead( byteLength ) );
				}else {
					// zlib compressed
					var compressed = new BytesInput( this.biRead( byteLength ) );
					new BytesInput( InflateImpl.run( compressed ) );
				};
				var readFunc = switch( c ) {
					case "b": function ( ) return FBXProperty.PBool( data.readByte( ) & 1 == 1 );
					case "i": function ( ) return FBXProperty.PInteger( data.readInt32( ) );
					case "l": function ( ) {
						var low = data.readInt32( );
						var high = data.readInt32( );
						return FBXProperty.PInteger( Int64.make( high, low ) );
					}
					case "f": function ( ) return FBXProperty.PFloat( data.readFloat( ) );
					case "d": function ( ) return FBXProperty.PFloat( data.readDouble( ) );
					default: throw "unknown";
				};
				var a = new Array<FBXProperty>( );
				for ( i in 0 ... length ) {
					a.push( readFunc( ) );
				}
				FBXProperty.PArray( a );
			default:
				throw "unknown data type: " + c;
		};
	}
}
