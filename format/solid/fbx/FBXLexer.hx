package format.solid.fbx;

import haxe.Int64;

enum FBXToken
{
	TEof;

	TIden( s:String );
	TBegin;
	TEnd;
	TComma;
	TColon;
	TStar;
	CFloat( f:Float );
	CInt( i:Int64 );
	CString( s:String );
}

/**
 * FBX用 レキサー
 * @author あるる（きのもと 結衣）
 */
class FBXLexer
{
	public var src( default, null ):String;
	public var len( default, null ):Int;
	public var line( default, null ):Int;
	public var p:Int;

	/**
	 * コンストラクタ
	 * @param src ソースコード
	 */
	public function new( src:String ) 
	{
		this.src = src;
		this.len = this.src.length;
		this.p = 0;
		this.line = 1;
	}

	/**
	 * error
	 * @param	msg
	 */
	private function error( msg:String ):Void
	{
		throw this.line + ":" + msg;
	}

	/**
	 * トークンがあるか？
	 * @return あるか否か
	 */
	public function hasNext( ):Bool
	{
		this.skipSpace( );
		return ( this.p < this.len );
	}

	/**
	 * コメントとスペースを読みとばす
	 */
	public function skipSpace( ):Void
	{
		var lineComment = false;
		var multiComment = false;

		while ( this.p < this.len ) {
			var through = false;
			var c:String = this.src.charAt( this.p );
			switch( c ) {
				case " ", "\t":
					through = true;
				case "\r":
					lineComment = false;
					through = true;
				case "\n":
					this.line ++;
					lineComment = false;
					through = true;
				case ";":
					lineComment = true;
			}

			if ( ( !lineComment ) && ( !multiComment ) && ( ! through ) ) break;
			this.p ++;
		}
	}

	/**
	 * トークン取得
	 * @return トークン
	 */
	public function next( ):FBXToken
	{
		this.skipSpace( );
		if ( this.len <= this.p ) return FBXToken.TEof;

		var c:String = this.src.charAt( this.p );

		this.p ++;
		switch( c ) {
			case "{": return FBXToken.TBegin;
			case "}": return FBXToken.TEnd;
			case ",": return FBXToken.TComma;
			case ":": return FBXToken.TColon;
			case "*": return FBXToken.TStar;
			case "\"": return this.getString( );
			case "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "-":
				return this.getNumber( );
		}

		return this.getIden( );
	}

	/**
	 * 数字取得
	 * @return 数字トークン
	 */
	private function getNumber( ):FBXToken
	{
		var str:StringBuf = new StringBuf( );
		var fFloat = false;

		this.p --;
		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( "-0123456789.xABCDEFabcdef".indexOf( c ) == -1 ) break;
			if ( c == "." ) fFloat = true;
			str.add( c );
			this.p ++;
		}

		try {
			var s = str.toString( );
			return if ( fFloat ) {
				FBXToken.CFloat( Std.parseFloat( s ) );
			}else {
				// Int64.parseStringが特定の数字でコケるので対応
				FBXToken.CInt( Int64.fromFloat( Std.parseFloat( s ) ) );
			};
		}catch ( d:Dynamic ) {
			throw "lexer error: invalid number";
		};
	}

	/**
	 * 文字列取得
	 * @return 数字トークン
	 */
	private function getString( ):FBXToken
	{
		var str:StringBuf = new StringBuf( );

		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( "\"" == c ) break;
			str.add( c );
			this.p ++;
		}
		this.p ++;

		return FBXToken.CString( str.toString( ) );
	}

	/**
	 * 識別子の取得
	 * @return 識別子トークンの取得
	 */
	private function getIden( ):FBXToken
	{
		var str:StringBuf = new StringBuf( );
		str.add( this.src.charAt( this.p - 1 ) );

		// 識別子になりうる文字一覧
		var table:String = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@";

		while ( this.p < this.len ) {
			var c = this.src.charAt( this.p );
			if ( table.indexOf( c ) == -1 ) break;
			str.add( c );
			this.p ++;
		}

		var s = str.toString( );
		return FBXToken.TIden( s );
	}

	/**
	 * 必要なトークン
	 * @param	t
	 */
	public function expected( e:FBXToken ):Void
	{
		var t = this.next( );
		if ( ! Type.enumEq( t, e ) ) {
			throw "expected :" + Std.string( e ) + " not " + Std.string( t );
		}
	}

	/**
	 * 1つのトークンを浮動小数点として読み込む
	 * @return
	 */
	public function readInteger( ):Int64
	{
		return switch( this.next( ) ) {
			case FBXToken.CInt( i ): i;
			default:
				throw "invalid integer";
		};
	}

	/**
	 * 1つのトークンを浮動小数点として読み込む
	 * @return
	 */
	public function readFloat( ):Float
	{
		return switch( this.next( ) ) {
			case FBXToken.CFloat( f ): f;
			case FBXToken.CInt( i ): Std.parseFloat( Int64.toStr( i ) );
			default:
				throw "invalid number";
		};
	}

	/**
	 * 1つのトークンを文字列として読み込む
	 * @return
	 */
	public function readString( ):String
	{
		return switch( this.next( ) ) {
			case FBXToken.CString( s ): s;
			default:
				throw "invalid string";
		};
	}

	/**
	 * 1つのトークンを識別子として読み込む
	 * @return
	 */
	public function readIden( ):String
	{
		return switch( this.next( ) ) {
			case FBXToken.TIden( s ): s;
			default:
				throw "invalid iden";
		};
	}
}
