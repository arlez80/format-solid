package format.solid.mqo;

import format.solid.Data;
import format.solid.Lexer;

class MQOLexerHelper
{

	/**
	 * 2つのトークンをUVとして読み込む
	 * @return
	 */
	static public function readUV( lex:Lexer ):SolidUV
	{
		var u = lex.readFloat( );
		var v = lex.readFloat( );
		return { u:u, v:v };
	}

	/**
	 * 3つのトークンを三角形として読み込む
	 * @return
	 */
	static public function readSurface3( lex:Lexer ):SolidSurface
	{
		var x = lex.readInteger( );
		var y = lex.readInteger( );
		var z = lex.readInteger( );
		return SolidSurface.triangle( x, y, z );
	}

	/**
	 * 4つのトークンを四角形として読み込む
	 * @return
	 */
	static public function readSurface4( lex:Lexer ):SolidSurface
	{
		var x = lex.readInteger( );
		var y = lex.readInteger( );
		var z = lex.readInteger( );
		var w = lex.readInteger( );
		return SolidSurface.rectangle( x, y, z, w );
	}

	/**
	 * 3つのトークンを座標として読み込む
	 * @return
	 */
	static public function readVertex( lex:Lexer ):SolidVertex
	{
		var x = lex.readFloat( );
		var y = lex.readFloat( );
		var z = lex.readFloat( );
		return { x:x, y:y, z:z };
	}

	/**
	 * 3つのトークンをRGBとして読み込む
	 * @return
	 */
	static public function readColor3( lex:Lexer ):SolidColor3
	{
		var r = lex.readFloat( );
		var g = lex.readFloat( );
		var b = lex.readFloat( );
		return { r:r, g:g, b:b };
	}

	/**
	 * 4つのトークンをRGBAとして読み込む
	 * @return
	 */
	static public function readColor4( lex:Lexer ):SolidColor4
	{
		var r = lex.readFloat( );
		var g = lex.readFloat( );
		var b = lex.readFloat( );
		var a = lex.readFloat( );
		return { r:r, g:g, b:b, a:a };
	}
}
