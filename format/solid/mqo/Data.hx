package format.solid.mqo;

import format.solid.Data;

typedef MQO = {
	version:String,
	scene: MQOScene,
	includeXmls:Array<String>,
	materials: Array<MQOMaterial>,
	objects: Array<MQOObject>,
};

typedef MQOScene = {
	pos:SolidVertex,
	lookAt:SolidVertex,
	head:Float,
	pich:Float,
	bank:Float,
	ortho:Bool,
	zoom2:Float,
	amb:SolidColor3,
	frontclip:Float,
	backclip:Float,
	dirLights:Array<MQOLight>
};

typedef MQOLight = {
	dir: SolidVertex,
	color: SolidColor3,
};

typedef MQOMaterial = {
	name:String,
	shader:Int,
	vcol:Bool,
	col:SolidColor4,
	dbls:Bool,
	dif:Float,
	amb:Float,
	emi:Float,
	spc:Float,
	power:Float,
	reflect:Float,
	refract:Float,
	tex:String,
	aplane:String,
	bump:String,
};

typedef MQOObject = {
	name:String,

	uid:Int,
	depth:Int,
	folding:Bool,

	scale:SolidVertex,
	rotation:SolidVertex,
	translation:SolidVertex,

	patch:Int,
	patchtri:Bool,
	patchsmoothtri:Int,
	patchmeshinterp:Int,
	patchuvinterp:Int,
	segment:Int,

	visible:Int,
	locking:Bool,
	shading:Int,
	facet:Float,
	color:SolidColor3,
	colorType:Int,
	mirror:Int,
	mirrorAxis:Int,
	mirrorDis:Float,
	lathe:Int,
	latheAxis:Int,
	latheSeg:Int,
	vertex:Array<SolidVertex>,
	face:Array<MQOFace>,
};

typedef MQOFace = {
	m:Int,
	v:SolidSurface,
	uv:Array<SolidUV>,
	col:Array<Int>,
	crs:Array<Bool>,
};
