package format.solid.mqo;

import format.solid.Data;
import format.solid.Lexer;
import format.solid.mqo.Data;
import haxe.io.Eof;
import haxe.io.Input;

using format.solid.mqo.MQOLexerHelper;

/**
 * .mqo Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	private var lex:Lexer;

	/**
	 * コンストラクタ
	 * @param	i
	 */
	public function new( bi:Input ) 
	{
		this.lex = new Lexer( bi.readAll( ).toString( ) );
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):MQO
	{
		var version = this.parseVersion( );
		var mqo = {
			version: version,
			scene: null,
			includeXmls: [],
			materials: null,
			objects: [],
		};

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "Scene" ): mqo.scene = this.parseScene( );
				case Token.TIden( "IncludeXml" ): mqo.includeXmls.push( this.lex.readString( ) );
				case Token.TIden( "Material" ): mqo.materials = this.parseMaterials( );
				case Token.TIden( "Object" ): mqo.objects.push( this.parseObject( ) );
				case Token.TIden( "Eof" ) | Token.TEof:
					break;
				case Token.TIden( _ ):
					this.skipBlock( );
				default:
					throw "invalid token: " + t;
			}
		}

		return mqo;
	}

	/**
	 * ブロックをスキップさせる
	 */
	private function skipBlock( ):Void
	{
		var block:Int = 0;

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TBegin:
					block ++;
				case Token.TEnd:
					if ( block == 0 ) throw "invalid block";
					block --;
					if ( block == 0 ) break;
				case Token.TEof:
					throw "invalid block";
				default:
					// 無視
			}
		}
	}

	/**
	 * バージョン取得
	 * @return
	 */
	private function parseVersion( ):String
	{	
		this.lex.expected( Token.TIden( "Metasequoia" ) );
		this.lex.expected( Token.TIden( "Document" ) );
		this.lex.expected( Token.TIden( "Format" ) );
		this.lex.expected( Token.TIden( "Text" ) );
		this.lex.expected( Token.TIden( "Ver" ) );

		return switch( this.lex.next( ) ) {
			case Token.CFloat( f ): Std.string( f );
			case Token.CInt( i ): Std.string( i );
			default:
				throw "invalid version.";
		};
	}

	/**
	 * Sceneチャンク取得
	 * @return
	 */
	private function parseScene( ):MQOScene
	{
		var scene:MQOScene = {
			pos:{x:0,y:0,z:1500},
			lookAt:{x:0,y:0,z:0},
			head:0.0,
			pich:0.0,
			bank:0.0,
			ortho:false,
			zoom2:1000.0,
			amb:{ r:0.25, g:0.25, b:0.25 },
			frontclip: 0.0,
			backclip: 0.0,
			dirLights: []
		};

		this.lex.expected( Token.TBegin );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TEnd: break;
				case Token.TIden( "pos" ): scene.pos = this.lex.readVertex( );
				case Token.TIden( "lookat" ): scene.lookAt = this.lex.readVertex( );
				case Token.TIden( "head" ): scene.head = this.lex.readFloat( );
				case Token.TIden( "pich" ): scene.pich = this.lex.readFloat( );
				case Token.TIden( "bank" ): scene.bank = this.lex.readFloat( );
				case Token.TIden( "ortho" ): scene.ortho = this.lex.readInteger( ) != 0;
				case Token.TIden( "zoom2" ): scene.zoom2 = this.lex.readFloat( );
				case Token.TIden( "amb" ): scene.amb = this.lex.readColor3( );
				case Token.TIden( "frontclip" ): scene.frontclip = this.lex.readFloat( );
				case Token.TIden( "backclip" ): scene.backclip = this.lex.readFloat( );
				case Token.TIden( "dirlights" ): scene.dirLights = this.parseSceneDirLights( );
				default:
					throw "invalid token:" + t;
			}
		}

		return scene;
	}

	/**
	 * SceneのdirLightをパースする
	 * @return
	 */
	private function parseSceneDirLights( ):Array<MQOLight>
	{
		var lights = new Array<MQOLight>( );

		var lightCount = this.lex.readInteger( );
		this.lex.expected( Token.TBegin );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TEnd: break;
				case Token.TIden( "light" ): lights.push( this.parseSceneDirLightsLight( ) );
				default:
					throw "invalid token:" + t;
			}
		}

		return lights;
	}

	/**
	 * SceneのdirLightのlightをパースする
	 * @return
	 */
	private function parseSceneDirLightsLight( ):MQOLight
	{
		var light = { color:{ r:1.0, g:1.0, b:1.0 }, dir:{ x:0.3, y:0.3, z:0.3 } };
		this.lex.expected( Token.TBegin );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TEnd: break;
				case Token.TIden( "dir" ): light.dir = this.lex.readVertex( );
				case Token.TIden( "color" ): light.color = this.lex.readColor3( );
				default:
					throw "invalid token:" + t;
			}
		}

		return light;
	}

	/**
	 * マテリアル読み込み
	 * @return
	 */
	private function parseMaterials( ):Array<MQOMaterial>
	{
		var materialCount = this.lex.readInteger( );
		this.lex.expected( Token.TBegin );

		var now:MQOMaterial = null;
		var materials = new Array<MQOMaterial>( );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TEnd: break;
				case Token.CString( name ):
					now = {
						name:name, shader:0, vcol:false, col: { r:0, g:0, b:0, a:0 },
						dbls: false,
						dif:0.0, amb:0.0, emi:0.0, spc:0.0, power:0.0,
						reflect: 0.0, refract: 1.0,
						tex:"", aplane:"", bump:""
					};
					materials.push(now);
				case Token.TIden( "shader" ):
					this.lex.expected( Token.TPOpen );
					now.shader = this.lex.readInteger( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "vcol" ):
					this.lex.expected( Token.TPOpen );
					now.vcol = this.lex.readInteger( ) != 0;
					this.lex.expected( Token.TPClose );
				case Token.TIden( "dbls" ):
					this.lex.expected( Token.TPOpen );
					now.dbls = this.lex.readInteger( ) != 0;
					this.lex.expected( Token.TPClose );
				case Token.TIden( "col" ):
					this.lex.expected( Token.TPOpen );
					now.col = this.lex.readColor4( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "dif" ):
					this.lex.expected( Token.TPOpen );
					now.dif = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "amb" ):
					this.lex.expected( Token.TPOpen );
					now.amb = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "emi" ):
					this.lex.expected( Token.TPOpen );
					now.emi = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "spc" ):
					this.lex.expected( Token.TPOpen );
					now.spc = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "power" ):
					this.lex.expected( Token.TPOpen );
					now.power = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "reflect" ):
					this.lex.expected( Token.TPOpen );
					now.reflect = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "refract" ):
					this.lex.expected( Token.TPOpen );
					now.refract = this.lex.readFloat( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "tex" ):
					this.lex.expected( Token.TPOpen );
					now.tex = this.lex.readString( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "aplane" ):
					this.lex.expected( Token.TPOpen );
					now.aplane = this.lex.readString( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "bump" ):
					this.lex.expected( Token.TPOpen );
					now.bump = this.lex.readString( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( _ ):
					while ( true ) {
						switch( this.lex.next( ) ) {
							case Token.TPClose: break;
							case Token.TEnd:
								throw "invalid material";
							case Token.TEof:
								throw "invalid material";
							default:
								// 無視
						}
					}
				default:
					throw "invalid token:" + t;
			}
		}

		return materials;
	}

	/**
	 * オブジェクトをパースする
	 * @return
	 */
	private function parseObject( ):MQOObject
	{
		var name = this.lex.readString( );
		this.lex.expected( Token.TBegin );

		var object:MQOObject = {
			name:name,

			uid:0,
			depth:0,
			folding:false,

			scale: { x:1, y:1, z:1 },
			rotation: { x:0, y:0, z:0 },
			translation: { x:0, y:0, z:0 },

			patch:0,
			patchtri:false,
			patchsmoothtri: 0,
			patchmeshinterp: 0,
			patchuvinterp: 0,
			segment:1,

			visible:15,
			locking:false,
			shading:0,
			facet:59.9,
			color: { r:1, g:1, b:1 },
			colorType:0,

			mirror:0,
			mirrorAxis:0,
			mirrorDis:0,

			lathe:0,
			latheAxis:0,
			latheSeg:0,

			vertex:[],
			face:[],
		};

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TEnd: break;
				case Token.TIden( "depth" ):
					object.depth = this.lex.readInteger( );
				case Token.TIden( "folding" ):
					object.folding = this.lex.readInteger( ) != 0;
				case Token.TIden( "scale" ):
					object.scale = this.lex.readVertex( );
				case Token.TIden( "rotation" ):
					object.rotation = this.lex.readVertex( );
				case Token.TIden( "translation" ):
					object.translation = this.lex.readVertex( );
				case Token.TIden( "patch" ):
					object.patch = this.lex.readInteger( );
				case Token.TIden( "patchtri" ):
					object.patchtri = this.lex.readInteger( ) != 0;
				case Token.TIden( "patchsmoothtri" ):
					object.patchsmoothtri = this.lex.readInteger( );
				case Token.TIden( "patchmeshinterp" ):
					object.patchmeshinterp = this.lex.readInteger( );
				case Token.TIden( "patchuvinterp" ):
					object.patchuvinterp = this.lex.readInteger( );
				case Token.TIden( "segment" ):
					object.segment = this.lex.readInteger( );
				case Token.TIden( "visible" ):
					object.visible = this.lex.readInteger( );
				case Token.TIden( "locking" ):
					object.locking = this.lex.readInteger( ) != 0;
				case Token.TIden( "shading" ):
					object.shading = this.lex.readInteger( );
				case Token.TIden( "facet" ):
					object.facet = this.lex.readFloat( );
				case Token.TIden( "color" ):
					object.color = this.lex.readColor3( );
				case Token.TIden( "color_type" ):
					object.colorType = this.lex.readInteger( );
				case Token.TIden( "mirror" ):
					object.mirror = this.lex.readInteger( );
				case Token.TIden( "mirrorAxis" ):
					object.mirrorAxis = this.lex.readInteger( );
				case Token.TIden( "mirrorDis" ):
					object.mirrorDis = this.lex.readInteger( );
				case Token.TIden( "lathe" ):
					object.lathe = this.lex.readInteger( );
				case Token.TIden( "latheAxis" ):
					object.latheAxis = this.lex.readInteger( );
				case Token.TIden( "latheSeg" ):
					object.latheSeg = this.lex.readInteger( );
				case Token.TIden( "vertex" ):
					var vertexCount = this.lex.readInteger( );
					this.lex.expected( Token.TBegin );
					for ( i in 0 ... vertexCount ) {
						object.vertex.push( this.lex.readVertex( ) );
					}
					this.lex.expected( Token.TEnd );
				case Token.TIden( "face" ):
					object.face = this.parseFaces( );
				case Token.TIden( s ):
					throw "unknown data:" + s;
				default:
					throw "invalid token:" + t;
			}
		}

		return object;
	}

	/**
	 * 面をパース
	 * @return
	 */
	private function parseFaces( ):Array<MQOFace>
	{
		var faceCount = this.lex.readInteger( );
		this.lex.expected( Token.TBegin );

		var now:MQOFace = null;
		var polygon = 0;
		var faces = new Array<MQOFace>( );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TEnd: break;
				case Token.CInt( i ):
					switch( i ) {
						case 3:
							now = {
								m:0,
								v:SolidSurface.triangle( 0, 0, 0 ),
								uv:[ { u:0, v:0 }, { u:0, v:0 }, { u:0, v:0 } ],
								col: [ 0, 0, 0 ],
								crs: [ false, false, false ]
							};
						case 4:
							now = {
								m:0,
								v:SolidSurface.rectangle( 0, 0, 0, 0 ),
								uv:[ { u:0, v:0 }, { u:0, v:0 }, { u:0, v:0 }, { u:0, v:0 } ],
								col: [ 0, 0, 0, 0 ],
								crs: [ false, false, false, false ]
							};
						default:
							throw "unknwon polygon count: " + i;
					}
					polygon = i;
					faces.push( now );
				case Token.TIden( "V" ):
					this.lex.expected( Token.TPOpen );
					if( polygon == 3 ) {
						now.v = this.lex.readSurface3( );
					}else {
						now.v = this.lex.readSurface4( );
					}
					this.lex.expected( Token.TPClose );
				case Token.TIden( "M" ):
					this.lex.expected( Token.TPOpen );
					now.m = this.lex.readInteger( );
					this.lex.expected( Token.TPClose );
				case Token.TIden( "UV" ):
					this.lex.expected( Token.TPOpen );
					for ( i in 0 ... polygon ) {
						now.uv[i] = this.lex.readUV( );
					}
					this.lex.expected( Token.TPClose );
				case Token.TIden( "COL" ):
					this.lex.expected( Token.TPOpen );
					for ( i in 0 ... polygon ) {
						now.col[i] = this.lex.readInteger( );
					}
					this.lex.expected( Token.TPClose );
				default:
					throw "unknown object data: " + t;
			}
		}

		return faces;
	}
}
