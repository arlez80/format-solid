package format.solid.mqo;

import format.solid.Data;
import format.solid.mqo.Data;
import haxe.io.Output;

/**
 * .mqo Writer
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	private var bo:Output;
	private var version:Int;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	/**
	 * 書き込む
	 * @param	mqo
	 */
	public function write( mqo:MQO )
	{
		this.writeHeader( mqo.version );

		this.writeScene( mqo.scene );
		this.writeMaterials( mqo.materials );

		for( o in mqo.objects ) {
			this.writeObject( o );
		}

		this.bo.writeString( "Eof" );
	}

	/**
	 * ヘッダ書き込み
	 * @param	version
	 */
	private function writeHeader( version:String ):Void
	{
		if ( version == "" ) version = "1.1";

		this.bo.writeString( "Metasequoia Document\r\n" );
		this.bo.writeString( "Format Text Ver " + version + "\r\n" );

		this.version = Std.parseInt( version.split( "." ).join( "" ) );
	}

	/**
	 * シーン情報書き込み
	 * @param	scene
	 */
	private function writeScene( scene:MQOScene ):Void
	{
		this.bo.writeString( "Scene {\r\n" );

		this.bo.writeString( "pos " );
		this.writeVertex( scene.pos );
		this.bo.writeString( "\r\n" );
		this.bo.writeString( "lookat " );
		this.writeVertex( scene.lookAt );
		this.bo.writeString( "\r\n" );
		this.bo.writeString( "head " + scene.head + "\r\n" );
		this.bo.writeString( "pich " + scene.pich + "\r\n" );
		this.bo.writeString( "bank " + scene.bank + "\r\n" );
		this.bo.writeString( "ortho " + scene.ortho + "\r\n" );
		this.bo.writeString( "zoom2 " + scene.zoom2 + "\r\n" );
		this.bo.writeString( "amb " );
		this.writeColor3( scene.amb );
		this.bo.writeString( "frontclip " + scene.frontclip + "\r\n" );
		this.bo.writeString( "backclip " + scene.backclip + "\r\n" );
		this.bo.writeString( "\r\n" );

		this.bo.writeString( "dirlights " + scene.dirLights.length + " {\r\n" );
		for ( light in scene.dirLights ) {
			this.bo.writeString( "light {\r\n" );
			this.bo.writeString( "dir " );
			this.writeVertex( light.dir );
			this.bo.writeString( "\r\n" );
			this.bo.writeString( "color " );
			this.writeColor3( light.color );
			this.bo.writeString( "\r\n" );
			this.bo.writeString( "}\r\n" );
		}
		this.bo.writeString( "}\r\n" );

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * マテリアル書き込み
	 * @param	materials
	 */
	private function writeMaterials( materials:Array<MQOMaterial> ):Void
	{
		this.bo.writeString( "Material " + materials.length + " {\r\n" );

		for ( mat in materials ) {
			this.bo.writeString( "\"" + mat.name + "\" " );
			this.bo.writeString( "col(" );
			this.writeColor4( mat.col );
			this.bo.writeString( ") " );
			this.bo.writeString( "vcol(" + ( mat.vcol ? 1 : 0 ) + ") " );
			this.bo.writeString( "dbls(" + ( mat.dbls ? 1 : 0 ) + ") " );
			this.bo.writeString( "dif(" + mat.dif + ") " );
			this.bo.writeString( "amb(" + mat.amb + ") " );
			this.bo.writeString( "emi(" + mat.emi + ") " );
			this.bo.writeString( "spc(" + mat.spc + ") " );
			this.bo.writeString( "power(" + mat.power + ") " );
			if( 11 <= this.version ) {
				this.bo.writeString( "reflect(" + mat.reflect + ") " );
				this.bo.writeString( "refract(" + mat.refract + ") " );
			}
			if( mat.tex != "" ) this.bo.writeString( "tex(\"" + mat.tex + "\") " );
			if( mat.aplane != "" ) this.bo.writeString( "aplane(\"" + mat.aplane + "\") " );
			if( mat.bump != "" ) this.bo.writeString( "bump(\"" + mat.bump + "\") " );
			this.bo.writeString( "\r\n" );
		}

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * オブジェクトを書き込む
	 * @param	o
	 */
	private function writeObject( o:MQOObject ):Void
	{
		this.bo.writeString( "Object \"" + o.name + "\" {\r\n" );

		this.bo.writeString( "uid " + o.uid + "\r\n" );
		this.bo.writeString( "depth " + o.depth + "\r\n" );
		this.bo.writeString( "folding " + ( o.folding ? 1 : 0 ) + "\r\n" );
		this.bo.writeString( "scale " );
		this.writeVertex( o.scale );
		this.bo.writeString( "\r\n" );
		this.bo.writeString( "rotation " );
		this.writeVertex( o.rotation );
		this.bo.writeString( "\r\n" );
		this.bo.writeString( "translation " );
		this.writeVertex( o.translation );
		this.bo.writeString( "\r\n" );
		this.bo.writeString( "patch " + o.patch + "\r\n" );
		this.bo.writeString( "patchtri " + ( o.patchtri ? 1 : 0 ) + "\r\n" );
		this.bo.writeString( "patchsmoothtri " + o.patchsmoothtri + "\r\n" );
		this.bo.writeString( "patchmeshinterp " + o.patchmeshinterp + "\r\n" );
		this.bo.writeString( "patchuvinterp " + o.patchuvinterp + "\r\n" );
		this.bo.writeString( "segment " + o.segment + "\r\n" );
		this.bo.writeString( "visible " + o.visible + "\r\n" );
		this.bo.writeString( "locking " + ( o.locking ? 1 : 0 ) + "\r\n" );
		this.bo.writeString( "shading " + o.shading + "\r\n" );
		this.bo.writeString( "facet " + o.facet + "\r\n" );
		this.bo.writeString( "color " );
		this.writeColor3( o.color );
		this.bo.writeString( "\r\n" );
		this.bo.writeString( "color_type " + o.colorType + "\r\n" );
		this.bo.writeString( "mirror " + o.mirror + "\r\n" );
		this.bo.writeString( "mirror_axis " + o.mirrorAxis + "\r\n" );
		this.bo.writeString( "mirror_dis " + o.mirrorDis + "\r\n" );
		this.bo.writeString( "lathe " + o.lathe + "\r\n" );
		this.bo.writeString( "lathe_axis " + o.latheAxis + "\r\n" );
		this.bo.writeString( "lathe_seg " + o.latheSeg + "\r\n" );

		this.bo.writeString( "vertex " + o.vertex.length + " {\r\n" );
		for ( v in o.vertex ) {
			this.writeVertex( v );
			this.bo.writeString( "\r\n" );
		}
		this.bo.writeString( "}\r\n" );

		this.bo.writeString( "face " + o.face.length + " {\r\n" );
		for ( f in o.face ) {
			switch( f.v ) {
				case SolidSurface.triangle( a, b, c ):
					this.bo.writeString( "3 V(" + a + " " + b + " " + c + ") " );
				case SolidSurface.rectangle( a, b, c, d ):
					this.bo.writeString( "4 V(" + a + " " + b + " " + c + " " + d + ") " );
			}
			this.bo.writeString( "M(" + f.m + ") " );
			this.bo.writeString( "UV(" );
			for ( uv in f.uv ) {
				this.bo.writeString( "" + uv.u + " " + uv.v + " " );
			}
			this.bo.writeString( ")" );
			if ( 11 <= this.version ) {
				if( f.col != null ) {
					this.bo.writeString( "COL(" );
					for ( col in f.col ) {
						this.bo.writeString( "" + col + " " );
					}
					this.bo.writeString( ") " );
				}
				if( f.crs != null ) {
					this.bo.writeString( "CRS(" );
					for ( crs in f.crs ) {
						this.bo.writeString( "" + crs + " " );
					}
					this.bo.writeString( ") " );
				}
			}

			this.bo.writeString( "\r\n" );
		}
		this.bo.writeString( "}\r\n" );

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * 座標を書き込む
	 * @param	v
	 */
	private function writeVertex( v:SolidVertex ):Void
	{
		this.bo.writeString( v.x + " " + v.y + " " + v.z );
	}

	/**
	 * 色(RGB)を書き込む
	 * @param	v
	 */
	private function writeColor3( v:SolidColor3 ):Void
	{
		this.bo.writeString( v.r + " " + v.g + " " + v.b );
	}

	/**
	 * 色(RGBA)を書き込む
	 * @param	v
	 */
	private function writeColor4( v:SolidColor4 ):Void
	{
		this.bo.writeString( v.r + " " + v.g + " " + v.b + " " + v.a );
	}
}
