package format.solid.x;

import format.solid.Data;
import format.solid.Lexer;
import format.solid.x.Data;
import haxe.io.Eof;
import haxe.io.Input;

using format.solid.x.XLexerHelper;

/**
 * .x Reader
 * @author あるる（きのもと 結衣）
 */
class Reader
{
	private var header:Array<String>;
	private var lex:Lexer;
	private var materials:Map<String,XMaterial>;

	/**
	 * コンストラクタ
	 * @param	i
	 */
	public function new( bi:Input ) 
	{
		this.header = bi.readLine( ).split( " " );
		this.materials = new Map<String,XMaterial>( );
		this.lex = new Lexer( bi.readAll( ).toString( ) );
	}

	/**
	 * 読み込む
	 * @return
	 */
	public function read( ):X
	{
		var x = {
			header:null,
			frames:new Array<XFrame>( ),
			animationSets:new Array<XAnimationSet>( ),
			animTicksPerSecond: 1,
		};

		x.header = this.readHeader( );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "Frame" ): x.frames.push( this.readFrame( ) );
				case Token.TIden( "Material" ):
					var t = this.lex.next( );
					var name = switch( t ) {
						case Token.TIden( name ): name;
						default:
							throw "invalid token: " + t;
					};
					this.materials.set( name, this.readMaterial( ) );
				case Token.TIden( "AnimationSet" ):
					x.animationSets.push( this.readAnimationSet( ) );
				case Token.TIden( "AnimTicksPerSecond" ):
					this.lex.expected( Token.TBegin );
					x.animTicksPerSecond = this.lex.readInteger( );
					this.lex.expected( Token.TSemicolon );
					this.lex.expected( Token.TEnd );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEof:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return x;
	}

	/**
	 * ブロックをスキップさせる
	 */
	private function skipBlock( ):Void
	{
		var block:Int = 0;

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TBegin:
					block ++;
				case Token.TEnd:
					if ( block == 0 ) throw "invalid block";
					block --;
					if ( block == 0 ) break;
				case Token.TEof:
					throw "invalid block";
				default:
					// 無視
			}
		}
	}

	/**
	 * ヘッダを読み込む
	 */
	private function readHeader( ):XHeader
	{
		var h = {
			major:0,
			minor:0,
			type:"",
			bit:0,
		};
		if( this.header[0] != "xof" ) {
			throw "invalid header" ;
		}

		h.major = Std.parseInt( this.header[1].substr( 0, 2 ) );
		h.minor = Std.parseInt( this.header[1].substr( 2, 2 ) );
		h.type = this.header[1].substr( 4 );

		h.bit = Std.parseInt( this.header[2] );

		return h;
	}

	/**
	 * フレーム読み込み
	 */
	private function readFrame( ):XFrame
	{
		var frame = {
			name:"",
			frameTransformMatrix:new Array<Float>( ),
			children: new Array<XFrame>( ),
			meshes: new Array<XMesh>( ),
		};

		frame.name = switch( this.lex.next( ) ) {
			case Token.TIden( s ):
				this.lex.expected( Token.TBegin );
				s;
			case Token.TBegin:
				"";
			default:
				throw "invalid frame name";
		};

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "FrameTransformMatrix" ):
					this.lex.expected( Token.TBegin );
					frame.frameTransformMatrix = this.lex.readFloatArray( );
					this.lex.expected( Token.TSemicolon );
					this.lex.expected( Token.TEnd );
				case Token.TIden( "Frame" ):
					frame.children.push( this.readFrame( ) );
				case Token.TIden( "Mesh" ):
					frame.meshes.push( this.readMesh( ) );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEnd:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return frame;
	}

	/**
	 * メッシュ読み込み
	 */
	private function readMesh( ):XMesh
	{
		var mesh = {
			name:"",
			vertex:null,
			surfaces:null,
			normals:null,
			texCoords:null,
			meshMaterialList:null,
			skinWeights:new Array<XSkinWeight>( ),
		};

		mesh.name = switch( this.lex.next( ) ) {
			case Token.TIden( s ):
				this.lex.expected( Token.TBegin );
				s;
			case Token.TBegin:
				"";
			default:
				throw "invalid mesh name";
		};

		// 頂点
		var counter = this.lex.readInteger( );
		lex.expected( Token.TSemicolon );
		mesh.vertex = this.lex.readVertexArray( );

		// 面
		var counter = this.lex.readInteger( );
		lex.expected( Token.TSemicolon );
		mesh.surfaces = this.lex.readSurfaceArray( );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "MeshMaterialList" ):
					mesh.meshMaterialList = this.readMeshMaterialList( );
				case Token.TIden( "MeshNormals" ):
					mesh.normals = this.readMeshNormals( );
				case Token.TIden( "MeshTextureCoords" ):
					mesh.texCoords = this.readMeshTextureCoords( );
				case Token.TIden( "SkinWeights" ):
					mesh.skinWeights.push( this.readSkinWeights( ) );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEnd:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return mesh;
	}

	/**
	 * MeshNormalsを読み込み
	 */
	private function readMeshNormals( ):XMeshNormals
	{
		this.lex.expected( Token.TBegin );

		var n = {
			normal:null,
			index:null,
		};

		// 法線
		var counter = this.lex.readInteger( );
		lex.expected( Token.TSemicolon );
		n.normal = this.lex.readVertexArray( );

		// 面
		var counter = this.lex.readInteger( );
		lex.expected( Token.TSemicolon );
		n.index = this.lex.readSurfaceArray( );

		this.lex.expected( Token.TEnd );

		return n;
	}

	/**
	 * UV座標を読み込み
	 * @return
	 */
	private function readMeshTextureCoords( ):XMeshTextureCoords
	{
		var texCoord = {
			uvs: null,
		};

		this.lex.expected( Token.TBegin );

		var counter = this.lex.readInteger( );
		lex.expected( Token.TSemicolon );
		texCoord.uvs = this.lex.readUVArray( );

		this.lex.expected( Token.TEnd );

		return texCoord;
	}

	/**
	 * マテリアルリストを読み込み
	 * @return
	 */
	private function readMeshMaterialList( ):XMeshMaterialList
	{
		var materialList = {
			index:null,
			materials:new Array<XMaterial>( ),
		};

		this.lex.expected( Token.TBegin );

		var materialCount = this.lex.readInteger( );
		this.lex.expected( Token.TSemicolon );
		var indexCount = this.lex.readInteger( );
		this.lex.expected( Token.TSemicolon );
		materialList.index = this.lex.readIntArray( );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "Material" ):
					materialList.materials.push( this.readMaterial( ) );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEnd:
					break;
				case Token.TBegin:
					var t = this.lex.next( );
					switch( t ) {
						case Token.TIden( name ):
							materialList.materials.push( this.materials.get( name ) );
						default:
							throw "invalid token: " + t;
					}
					this.lex.expected( Token.TEnd );
				default:
					throw "invalid token: " + t;
			}
		}

		return materialList;
	}

	/**
	 * マテリアル読み込み
	 */
	private function readMaterial( ):XMaterial
	{
		var material = {
			diffuse:null,
			power:0.0,
			specular:null,
			emissive:null,
			tex:null,
		};

		this.lex.expected( Token.TBegin );

		material.diffuse = this.lex.readColor4( );
		this.lex.expected( Token.TSemicolon );
		material.power = this.lex.readFloat( );
		this.lex.expected( Token.TSemicolon );
		material.specular = this.lex.readColor3( );
		this.lex.expected( Token.TSemicolon );
		material.emissive = this.lex.readColor3( );
		this.lex.expected( Token.TSemicolon );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "TextureFilename" ):
					this.lex.expected( Token.TBegin );
					material.tex = this.lex.readString( );
					this.lex.expected( Token.TSemicolon );
					this.lex.expected( Token.TEnd );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEnd:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return material;
	}

	/**
	 * スキンウェイト読み込み
	 */
	private function readSkinWeights( ):XSkinWeight
	{
		var skinWeights = {
			target:"",
			index:new Array<Int>(),
			weight:new Array<Float>(),
			matrix:new Array<Float>(),
		};

		this.lex.expected( Token.TBegin );

		skinWeights.target = this.lex.readString( );
		this.lex.expected( Token.TSemicolon );

		var count = this.lex.readInteger( );
		this.lex.expected( Token.TSemicolon );

		// インデックス
		skinWeights.index = this.lex.readIntArray( );

		// ウェイト
		skinWeights.weight = this.lex.readFloatArray( );

		// 行列
		skinWeights.matrix = this.lex.readFloatArray( );
		this.lex.expected( Token.TSemicolon );

		this.lex.expected( Token.TEnd );

		return skinWeights;
	}

	/**
	 * アニメーションセット読み込み
	 */
	private function readAnimationSet( ):XAnimationSet
	{
		var animationSets = {
			name:"",
			animations:new Array<XAnimation>( ),
		};

		animationSets.name = switch( this.lex.next( ) ) {
			case Token.TIden( s ): s;
			default:
				throw "invalid frame name";
		};

		this.lex.expected( Token.TBegin );

		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TIden( "Animation" ):
					animationSets.animations.push( this.readAnimation( ) );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEnd:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return animationSets;
	}

	/**
	 * アニメーション読み込み
	 */
	private function readAnimation( ):XAnimation
	{
		var animation = {
			name:"",
			target:"",
			keys:new Array<XAnimationKey>( ),
		};

		var t = this.lex.next( );
		animation.name = switch( t ) {
			case Token.TIden( s ):
				this.lex.expected( Token.TBegin );
				s;
			case Token.TBegin:
				"";	// 名前なし
			default:
				throw "invalid frame name: " + t;
		};


		while ( true ) {
			var t = this.lex.next( );
			switch( t ) {
				case Token.TBegin:
					animation.target = this.lex.readIden( );
					this.lex.expected( Token.TEnd );
				case Token.TIden( "AnimationKey" ):
					animation.keys.push( this.readAnimationKey( ) );
				case Token.TIden( _ ):
					this.skipBlock( );
				case Token.TEnd:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return animation;
	}

	/**
	 * アニメーションキー読み込み
	 */
	private function readAnimationKey( ):XAnimationKey
	{
		var animationKey = {
			type:null,
			data:new Array<XAnimationKeyData>( ),
		};

		this.lex.expected( Token.TBegin );

		animationKey.type = switch( this.lex.readInteger( ) ) {
			case 0: XAnimationKeyType.scale;
			case 1: XAnimationKeyType.position;
			case 2: XAnimationKeyType.rotation;
			case 3: XAnimationKeyType.matrix;
			case 4: XAnimationKeyType.matrix;
			default:
				throw "unknown animation key type";
		};
		this.lex.expected( Token.TSemicolon );

		var counter = this.lex.readInteger( );
		this.lex.expected( Token.TSemicolon );
		for( i in 0 ... counter ) {
			var keyData = {
				frame:0,
				data:new Array<Float>( ),
			};

			keyData.frame = this.lex.readInteger( );
			this.lex.expected( Token.TSemicolon );
			// データ数
			this.lex.readInteger( );
			this.lex.expected( Token.TSemicolon );
			// データ読み込み
			keyData.data = this.lex.readFloatArray( );
			animationKey.data.push( keyData );

			// 締め
			this.lex.expected( Token.TSemicolon );
			var t = this.lex.next( );
			switch( t ) {
				case Token.TComma:
					// 続く
				case Token.TSemicolon:
					// 終了
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		this.lex.expected( Token.TEnd );

		return animationKey;
	}
}
