package format.solid.x;

import format.solid.Data;

typedef X = {
	// ヘッダ
	header:XHeader,

	//  データ
	frames:Array<XFrame>,
	animationSets:Array<XAnimationSet>,
	animTicksPerSecond:Int,
};

typedef XHeader = {
	major:Int,
	minor:Int,
	type:String,
	bit:Int,
};

typedef XFrame = {
	name:String,
	frameTransformMatrix:Array<Float>,
	children:Array<XFrame>,
	meshes:Array<XMesh>,
};

typedef XMesh = {
	name:String,
	vertex:Array<SolidVertex>,
	surfaces:Array<SolidSurface>,
	normals:XMeshNormals,
	texCoords:XMeshTextureCoords,
	meshMaterialList:XMeshMaterialList,
	skinWeights:Array<XSkinWeight>,
};

typedef XMeshNormals = {
	normal:Array<SolidVertex>,
	index:Array<SolidSurface>,
};

typedef XMeshTextureCoords = {
	uvs:Array<SolidUV>
};

typedef XMeshMaterialList = {
	index:Array<Int>,
	materials:Array<XMaterial>,
};

typedef XMaterial = {
	diffuse:SolidColor4,
	power:Float,
	specular:SolidColor3,
	emissive:SolidColor3,
	tex:String,
};

typedef XSkinWeight = {
	target:String,
	index:Array<Int>,
	weight:Array<Float>,
	matrix:Array<Float>,
};

typedef XAnimationSet = {
	name:String,
	animations:Array<XAnimation>,
};

typedef XAnimation = {
	name:String,
	target:String,
	keys:Array<XAnimationKey>,
};

typedef XAnimationKey = {
	type:XAnimationKeyType,
	data:Array<XAnimationKeyData>,
};

enum XAnimationKeyType {
	scale;
	position;
	rotation;
	matrix;
}

typedef XAnimationKeyData = {
	frame:Int,
	data:Array<Float>,
};
