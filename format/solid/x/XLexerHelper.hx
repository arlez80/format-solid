package format.solid.x;

import format.solid.Data;
import format.solid.Lexer;

class XLexerHelper
{
	/**
	 * RGBを読み込む
	 */
	static public function readColor3( lex:Lexer ):SolidColor3
	{
		var r = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var g = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var b = lex.readFloat( );
		lex.expected( Token.TSemicolon );

		return {r:r, g:g, b:b};
	}

	/**
	 * RGBAを読み込む
	 */
	static public function readColor4( lex:Lexer ):SolidColor4
	{
		var r = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var g = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var b = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var a = lex.readFloat( );
		lex.expected( Token.TSemicolon );

		return {r:r, g:g, b:b, a:a};
	}

	/**
	 * 頂点を読み込む
	 */
	static public function readVertex( lex:Lexer ):SolidVertex
	{
		var x = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var y = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var z = lex.readFloat( );
		lex.expected( Token.TSemicolon );

		return {x:x, y:y, z:z};
	}

	/**
	 * 頂点を読み込む
	 */
	static public function readUV( lex:Lexer ):SolidUV
	{
		var u = lex.readFloat( );
		lex.expected( Token.TSemicolon );
		var v = lex.readFloat( );
		lex.expected( Token.TSemicolon );

		return {u:u, v:v};
	}

	/**
	 * 面を読み込む
	 */
	static public function readSurface( lex:Lexer ):SolidSurface
	{
		var counter = lex.readInteger( );
		lex.expected( Token.TSemicolon );

		return switch( counter ) {
			case 3:
				var a = lex.readInteger( );
				lex.expected( Token.TComma );
				var b = lex.readInteger( );
				lex.expected( Token.TComma );
				var c = lex.readInteger( );
				lex.expected( Token.TSemicolon );
				SolidSurface.triangle( a, b, c );
			case 4:
				var a = lex.readInteger( );
				lex.expected( Token.TComma );
				var b = lex.readInteger( );
				lex.expected( Token.TComma );
				var c = lex.readInteger( );
				lex.expected( Token.TComma );
				var d = lex.readInteger( );
				lex.expected( Token.TSemicolon );
				SolidSurface.rectangle( a, b, c, d );
			default:
				throw "invalid vertex count on surface";
		};
	}

	/**
	 * Vertex配列を読み込む
	 */
	static public function readVertexArray( lex:Lexer ):Array<SolidVertex>
	{
		var a = new Array<SolidVertex>( );

		while ( true ) {
			a.push( XLexerHelper.readVertex( lex ) );

			var t = lex.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * Surface配列を読み込む
	 */
	static public function readSurfaceArray( lex:Lexer ):Array<SolidSurface>
	{
		var a = new Array<SolidSurface>( );

		while ( true ) {
			a.push( XLexerHelper.readSurface( lex ) );

			var t = lex.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * UV配列を読み込む
	 */
	static public function readUVArray( lex:Lexer ):Array<SolidUV>
	{
		var a = new Array<SolidUV>( );

		while ( true ) {
			a.push( XLexerHelper.readUV( lex ) );

			var t = lex.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * Int配列を読み込む
	 */
	static public function readIntArray( lex:Lexer ):Array<Int>
	{
		var a = new Array<Int>( );

		while ( true ) {
			var t = lex.next( );
			switch( t ) {
				case Token.CInt( i ):
					a.push( i );
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}

			var t = lex.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}

	/**
	 * Float配列を読み込む
	 */
	static public function readFloatArray( lex:Lexer ):Array<Float>
	{
		var a = new Array<Float>( );

		while ( true ) {
			var t = lex.next( );
			switch( t ) {
				case Token.CFloat( f ):
					a.push( f );
				case Token.CInt( i ):
					a.push( i );
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}

			var t = lex.next( );
			switch( t ) {
				case Token.TComma:
				case Token.TSemicolon:
					break;
				default:
					throw "invalid token: " + t;
			}
		}

		return a;
	}
}
