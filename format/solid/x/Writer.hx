package format.solid.x;

import format.solid.Data;
import format.solid.x.Data;
import haxe.io.Output;

/**
 * .x Writer
 * @author あるる（きのもと 結衣）
 */
class Writer 
{
	private var bo:Output;

	/**
	 * コンストラクタ
	 * @param	bo
	 */
	public function new( bo:Output ) 
	{
		this.bo = bo;
	}

	/**
	 * 書き込み開始
	 * @param	x
	 */
	public function write( x:X ):Void
	{
		this.writeHeader( );
		this.writeAnimTicksPerSecond( x.animTicksPerSecond );

		for( f in x.frames ) {
			this.writeFrame( f );
		}

		for ( a in x.animationSets ) {
			this.writeAnimationSet( a );
		}
	}

	/**
	 * ヘッダ書き込み
	 */
	private function writeHeader( ):Void
	{
		this.bo.writeString( "xof 0303txt 0032\r\n" );
	}

	/**
	 * アニメ再生速度の書き込み
	 * @param	aps
	 */
	private function writeAnimTicksPerSecond( aps:Int ):Void
	{
		this.bo.writeString( "AnimTicksPerSecond { " + aps + "; }\r\n" );
	}

	/**
	 * フレーム書き込み
	 * @param	f
	 */
	private function writeFrame( frame:XFrame ):Void
	{
		this.bo.writeString( "Frame " + frame.name + " {\r\n" );

		if ( frame.frameTransformMatrix != null ) {
			this.bo.writeString( "FrameTransformMatrix {\r\n" );
			this.bo.writeString( frame.frameTransformMatrix.join( "," ) );
			this.bo.writeString( ";;}\r\n" );
		}

		for ( f in frame.children ) {
			this.writeFrame( f );
		}
		for ( m in frame.meshes ) {
			this.writeMesh( m );
		}

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * メッシュ書き込み
	 * @param	mesh
	 */
	private function writeMesh( mesh:XMesh ):Void
	{
		this.bo.writeString( "Mesh " + mesh.name + " {\r\n" );

		this.writeVertexes( mesh.vertex );
		this.writeSurfaces( mesh.surfaces );

		if ( mesh.normals != null ) {
			this.bo.writeString( "MeshNormals {\r\n" );
			this.writeVertexes( mesh.normals.normal );
			this.writeSurfaces( mesh.normals.index );
			this.bo.writeString( "}\r\n" );
		}

		if ( mesh.texCoords != null ) {
			this.bo.writeString( "MeshTextureCoords {" );
			this.writeTexCoords( mesh.texCoords.uvs );
			this.bo.writeString( "}\r\n" );
		}

		if ( mesh.meshMaterialList != null ) {
			this.writeMeshMaterialList( mesh.meshMaterialList );
		}

		for ( sw in mesh.skinWeights ) {
			this.writeSkinWeights( sw );
		}

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * マテリアルリストを書き込む
	 * @param	meshMaterialList
	 */
	private function writeMeshMaterialList( meshMaterialList:XMeshMaterialList ):Void
	{
		this.bo.writeString( "MeshMaterialList {" );

		this.bo.writeString( Std.string( meshMaterialList.materials.length ) + ";\r\n" );
		this.writeIntArray( meshMaterialList.index );
		for( m in meshMaterialList.materials ) {
			this.writeMaterial( m );
		}

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * マテリアル
	 * @param	m
	 */
	private function writeMaterial( m:XMaterial ):Void
	{
		this.bo.writeString( "Material {" );

		this.bo.writeString( "" + m.diffuse.r + ";" + m.diffuse.g + ";" + m.diffuse.b + ";" + m.diffuse.a + ";;" );
		this.bo.writeString( "" + m.power + ";" );
		this.bo.writeString( "" + m.specular.r + ";" + m.specular.g + ";" + m.specular.b + ";;" );
		this.bo.writeString( "" + m.emissive.r + ";" + m.emissive.g + ";" + m.emissive.b + ";;" );

		if ( m.tex != null ) {
			this.bo.writeString( "\r\nMaterial {" );
			this.bo.writeString( "\"" + m.tex + "\"" );
			this.bo.writeString( "}\r\n" );
		}

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * スキンウェイトを書き込む
	 * @param	sw
	 */
	private function writeSkinWeights( sw:XSkinWeight ):Void
	{
		this.bo.writeString( "SkinWeights {" );
		this.bo.writeString( "\"" + sw.target + "\";" );

		this.writeIntArray( sw.index );
		var buf = new Array<String>( );
		for ( t in sw.weight ) {
			buf.push( t + ";" );
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );

		var buf = new Array<String>( );
		for ( t in sw.matrix ) {
			buf.push( t + ";" );
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * アニメーションセット書き込む
	 * @param	as
	 */
	private function writeAnimationSet( as:XAnimationSet ):Void
	{
		this.bo.writeString( "AnimationSet " + as.name + " {" );

		for ( a in as.animations ) {
			this.writeAnimation( a );
		}

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * アニメーション書き込む
	 * @param	a
	 */
	private function writeAnimation( a:XAnimation ):Void
	{
		this.bo.writeString( "Animation " + a.name + " {" );

		for ( key in a.keys ) {
			this.writeAnimationKey( key );
		}

		this.bo.writeString( "{" + a.target + "}" );
		this.bo.writeString( "}\r\n" );
	}

	/**
	 * アニメーションキー書き込む
	 * @param	ak
	 */
	private function writeAnimationKey( ak:XAnimationKey ):Void
	{
		this.bo.writeString( "AnimationKey {" );

		var type = switch( ak.type ) {
			case XAnimationKeyType.scale: 0;
			case XAnimationKeyType.position: 1;
			case XAnimationKeyType.rotation: 2;
			case XAnimationKeyType.matrix: 3;
			default:
				throw "unknown animation key type";
		};
		this.bo.writeString( "" + type + ";" );

		this.bo.writeString( "" + ak.data.length + ";" );
		var buf = new Array<String>( );
		for ( t in ak.data ) {
			buf.push( "" + t.frame + ";" + t.data.length + ";" + t.data.join( "," ) + ";;" );
		}
		this.bo.writeString( "" + buf.join( "," ) + ";" );

		this.bo.writeString( "}\r\n" );
	}

	/**
	 * 整数配列を書き込む
	 * @param	d
	 */
	private function writeIntArray( a:Array<Int> ):Void
	{
		this.bo.writeString( Std.string( a.length ) + ";\r\n" );
		var buf = new Array<String>( );
		for ( t in a ) {
			buf.push( t + ";" );
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );
	}

	/**
	 * 浮動小数配列を書き込む
	 * @param	d
	 */
	private function writeFloatArray( a:Array<Float> ):Void
	{
		this.bo.writeString( Std.string( a.length ) + ";\r\n" );
		var buf = new Array<String>( );
		for ( t in a ) {
			buf.push( t + ";" );
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );
	}

	/**
	 * 頂点配列を書き込む
	 * @param	d
	 */
	private function writeVertexes( vertexes:Array<SolidVertex> ):Void
	{
		this.bo.writeString( Std.string( vertexes.length ) + ";\r\n" );
		var buf = new Array<String>( );
		for ( v in vertexes ) {
			buf.push( "" + v.x + ";" + v.y + ";" + v.z + ";" );
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );
	}

	/**
	 * 面データを書き込む
	 * @param	d
	 */
	private function writeSurfaces( surfaces:Array<SolidSurface> ):Void
	{
		this.bo.writeString( Std.string( surfaces.length ) + ";\r\n" );
		var buf = new Array<String>( );
		for ( s in surfaces ) {
			switch( s ) {
				case SolidSurface.triangle( a, b, c ):
					buf.push( "3;" + a + "," + b + "," + c + ";" );
				case SolidSurface.rectangle( a, b, c, d ):
					buf.push( "4;" + a + "," + b + "," + c + "," + d + ";" );
			}
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );
	}

	/**
	 * UV座標書き込む
	 * @param	uvs
	 */
	private function writeTexCoords( uvs:Array<SolidUV> ):Void
	{
		this.bo.writeString( Std.string( uvs.length ) + ";\r\n" );
		var buf = new Array<String>( );
		for ( uv in uvs ) {
			buf.push( uv.u + ";" + uv.v + ";" );
		}
		this.bo.writeString( buf.join( "," ) + ";\r\n" );
	}
}
